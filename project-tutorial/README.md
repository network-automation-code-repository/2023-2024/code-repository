# Network Automation project tutorial

## Getting started with Mininet

- [ ] Display startup options
```
sudo mn –h
```

- [ ] Run a simple topology: 1 Openflow switch, 2 hosts
```
sudo mn
```

- [ ] Play with mininet
```
mininet>help
mininet>nodes
mininet>net
mininet>dump
mininet>h1 ifconfig –a
mininet> h1 ping -c 1 h2
mininet>pingall
mininet>exit
```

- [ ] if mininet crashes
```
sudo mn -c
```

- [ ] Run network traffic between hosts. For instance: h1 <-> h2. Open two terminals and run the following commands:
- - [ ] Terminal 1: 	
```
m h1 iperf -s
```
- - [ ] Terminal 2: 	
```
m h2 iperf -c 10.0.0.1
```


## Use custom topologies with Mininet

- [ ] Start mininet with a custom topology "one_switch" defined into mininet-topologies/ folder
```
sudo mn --custom mininet-topologies/one_switch.py --mac --topo mytopo --controller=remote,ip=127.0.0.1,port=6633 --switch ovs,protocols=OpenFlow13
```

- [ ] Start mininet with a custom topology "topology_1" defined into mininet-topologies/ folder
```
sudo mn --custom mininet-topologies/topology_1.py --mac --pre mininet-topologies/config_1 --topo mytopo --controller=remote,ip=127.0.0.1,port=6633 --switch ovs,protocols=OpenFlow13
```

## Install Openflow rules manually into the openvswitches

- [ ] Open a terminal and run the following commands 
- - [ ] Show the openflow switches:
```
sudo ovs-vsctl show
```
- - [ ] Show the openflow rules of switche s1:
```
sudo ovs-ofctl -O OpenFlow13 dump-flows s1
```
- - [ ] Add the following rule to s1:
```
sudo ovs-ofctl -O OpenFlow13 add-flow s1 in_port=s1-eth1,actions=s1-eth2
```

## Start the RYU SDN Controller with one (or more) applications

- [ ] In order to start RYU with specific applications, run the following commands:

```
ryu-manager flowmanager/flowmanager.py my-first-app.py
```

- [ ] The flowmanager application can be viewed by connecting to this [link](http://localhost:8080/home/index.html)
