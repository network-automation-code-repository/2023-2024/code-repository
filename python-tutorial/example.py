# First example for students
d = 2
print(d)

# Second example: if/else statement
if d == 2:
	print('yes, you right')
else:
	print('nop')


# Third example: for loop

list_1 = ['first', 'lunch', 3, 'polimi']

for i in list_1:
	print(i)


